const express = require('express')
const router = express.Router()

// Import all controller exported functions
const {
    register,
    getAllUsers,
    checkEmail,
    login,
    profile,
    update,
    updatePassword,
    adminStatus,
    isUser,
    deleteUser
} = require('./../controllers/userController')

const {
    verify, 
    decode,
    verifyAdmin
} = require('../auth')
// const { update } = require('../models/Users')

// GET / SHOW user
router.get('/', async (req,res) => {
    try {
        await getAllUsers().then(result=>res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

// REGISTER a user
router.post('/register', async (req,res) => {
    // console.log(req.body)
    // .then() will handle the result of the promise / register()
//  await register(req.body).then(result => res.send(result))

    try {
        await register(req.body).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

// CHECK IF EMAIL EXISTS
router.post('/email-exists', async (req,res) => {
    try {
        await checkEmail(req.body).then(result=>res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

// LOGIN THE USER
    // authentication
router.post('/login', async (req,res) => {
    try{
        await login(req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// RETRIEVE USER INFORMATION
router.get('/profile', verify, async (req,res) => {
    // check the value in headers
    console.log(req.headers)

    const userId = decode(req.headers.authorization).id
    // check if userId has the decoded payload
    console.log(userId)
    // res.send(`Welcome to get request`)
    try{
        // token is located at header
        await profile(req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//update user information
router.put('/update', verify, async(req,res) =>{
    const userId = decode(req.headers.authorization).id
    try{
        await update(userId, req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//update user password
router.patch('/update-password', verify, async(req,res) => {
    const userId = decode(req.headers.authorization).id
    try{
        if(req.body.password === req.body.repassword){
            await updatePassword(userId, req.body.password).then((result) => res.send(result))
        }
    }catch(err){
        res.status(500).json(err)
    }
})

// change isAdmin to a user
router.patch('/isAdmin', verify, async (req,res) => {
    // console.log(decode(req.headers.authorization).id)
    // console.log(decode(req.headers.authorization).isAdmin)
    const admin = decode(req.headers.authorization).isAdmin
    try{
        if(admin == true){
            await adminStatus(req.body).then(result=>res.send(result))
        }else{
            res.send(`You are not authorized`)
        }
    }catch(err){
        res.send(`you are not authorized`)
    }
})
// change isAdmin to a user w/ middleware
// router.patch('/isAdmin', verifyAdmin, async (req,res) => {
//     // console.log(decode(req.headers.authorization).id)
//     // console.log(decode(req.headers.authorization).isAdmin)
//     const admin = decode(req.headers.authorization).isAdmin
//     try{
//         await adminStatus(req.body).then(result=>res.send(result))
//     }catch(err){
//         res.status(500).json(err)
//     }
// })

router.patch('/isUser', verifyAdmin, async(req,res) => {
    try{
        await isUser(req.body).then(result=>res.send(result))
    }catch(err){
        res.status(500).send(err)
    }
})

// delete a user from the DB
router.delete('/delete-user', verifyAdmin, async(req,res) => {
    try{
        await deleteUser(req.body).then(result=>res.send(result))
    }catch(err){
        res.status(500).send(err)
    }
})


// Export the router module to be used in index.js or main js file
// Mandatory to declate at the end of the router 
module.exports = router