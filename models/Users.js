const mongoose = require('mongoose')

// User Schema
const userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: [true, `Firstname is required`]
    },
    lastname: {
        type: String,
        required: [true, `Lastname is required`]
    },
    email: {
        type: String,
        required: [true, `Email is required`],
        unique: true
    },
    password: {
        type: String,
        required: [true, `Password is required`]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    // array of enrollement
    enrollment: [
        // index 0
        {
            courseId: {
                courseId: String,
                // required: [true, `Course ID is required`]
            },
            status: {
                type: String,
                default: "Enrolled"
            },
            enrolleOn: {
                type: Date,
                default: new Date()
            }
            
        }
        // index 1
    ]

}, {timestamps:true} ) // 2nd parameter is timestamps

// if the model (User) is exported no need to asign in a var
// User -> model name
module.exports = mongoose.model(`User`, userSchema)