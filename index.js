const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const dotenv = require('dotenv').config()

// PORT
const PORT = process.env.PORT || 3007
// server
const APP = express()

// Connect our routes module. userRoutes
const userRouter = require('./routes/userRoutes')

// Middleware to handle JSON payloads
APP.use(express.json())
APP.use(express.urlencoded({extended:true}))
// cors() prevents blocking of request from client esp different domains
APP.use(cors())

// db connection
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});
// db connection error check
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => console.log(`Connected to Database`))
  // we're connected!

// Schema
    // schema is in model module

// Routes
    // create a middleware to be the root url of all routes
APP.use(`/api/users`, userRouter) // every url endpoint is transfered at userRouter


APP.listen(PORT, ()=>console.log(`Connected to ${PORT}`))