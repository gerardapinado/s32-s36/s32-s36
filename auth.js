const jwt = require('jsonwebtoken')

// sign
    // generate a token
module.exports.createToken = (data) => {
    let userData = {
        id: data._id,
        email: data.email,
        isAdmin: data.isAdmin
    }

    return jwt.sign(userData, process.env.SECRET_PASS)
}

// verify
    // verify a token
    // 3rd param refers to middleware
module.exports.verify = (req, res, next) => {
    const requestToken = req.headers.authorization
    // check if requestToken has the token
    // remove the "bearer " from the authentication property
    const token = requestToken.slice(7, requestToken.length)
    console.log(requestToken)

    if(typeof requestToken == "undefined"){
        res.status(401).send(`Token Missing`)
    }
    else {
        const token = requestToken.slice(7, requestToken.length)
        console.log(requestToken)

        if(typeof token !== "undefined"){
            return jwt.verify(token, process.env.SECRET_PASS, (data,err) => {
                if(err){
                    return res.send({auth: `auth failed!`})
                }
                else {
                    next()
                }
            })
        }
    }
}

// decode token
module.exports.decode = (bearerToken) => {
    const token = bearerToken.slice(7, bearerToken.length)

    return jwt.decode(token)
}

// isAdmin Access
module.exports.verifyAdmin = (req, res, next) => {
    const requestToken = req.headers.authorization
    // check if requestToken has the token
    // remove the "bearer " from the authentication property
    const token = requestToken.slice(7, requestToken.length)
    console.log(requestToken)

    if(typeof requestToken == "undefined"){
        res.status(401).send(`Token Missing`)
    }
    else {
        const token = requestToken.slice(7, requestToken.length)
        console.log(requestToken)

        if(typeof token !== "undefined"){
            const admin = jwt.decode(token).isAdmin
            if(admin){
                return jwt.verify(token, process.env.SECRET_PASS, (data,err) => {
                    if(err){
                        return res.send({auth: `auth failed!`})
                    }
                    else {
                        next()
                    }
                })
            }
            else{
                res.status(403).send(`You are not Authorized`)
            }
        }
    }
}