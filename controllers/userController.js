const Users = require("./../models/Users")
// import crypto-js 
const cryptojs = require('crypto-js')
// import bcrypt
const bcrypt = require('bcrypt')
// salt round is the number of time of encryption
const saltRounds = 10
// jwt
const jwt = require('jsonwebtoken')
const { createToken } = require("../auth")
const { findOneAndUpdate, findOneAndDelete } = require("./../models/Users")

// REGISTER a user
module.exports.register = async (reqBody) => {
    // return await -> promise
    // return await .save(reqBody)
    // console.log(reqBody)

    const {firstName, lastName, email, password} = reqBody
    const newUser = new User ({
        firstname: firstName,
        lastname: lastName,
        email: email,
        // encrypting password using crypto-js
        password: cryptojs.AES.encrypt(password, process.env.SECRET_PASS).toString()
        // hasing password using bcrypt
        // password: bcrypt.hashSync(password, saltRounds);
    })

    return await newUser.save().then(result => {
        if(result){
            return true
        } else {
            if(result==null){
                return false
            }
        }
    })
}

// GET all users
module.exports.getAllUsers = async () => {
    // .then() will handle the find() promise
    return await User.find().then( result => result)
}

// CHECK IF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) => {
    // obj deconstruct
    const {email} = reqBody
    // findOne() return a promise
    // then() handles that promise
    return await User.findOne({email: email}).then((res,err) => {
        if(result){
            return true
        }else{
            if(result==null){
                return false
            }else{
                return err
            }
        }
    })
}

// LOGIN user
module.exports.login = async (reqBody) => {
    return await User.findOne({email: reqBody.email}).then(result => {
        if(result==null){
            return {message: `User does not exist.`}
        }
        else {
            if(result !== null){
                // decrypt the password from the database
                var bytes  = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS)
                var decryptedPw = bytes.toString(CryptoJS.enc.Utf8)
                // var decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
                // check if pw is correct
                if(reqBody.password == decryptedPw){
                    // .log - verify if condition is true
                    // console.log(`create token`) 

                    // create a user token via createToken()
                    return {token: createToken(result)}
                }
                else {
                    return {auth: `Authentication Failed!`}
                }
            }
            else {
                return err
            }
        }
    })
}

// RETRIEVE USER INFORMATION
module.exports.process = async(id) => {
    return await User.findById(id).then(result => {
        if(result){
            return result
        } else {
            if(result == null){
                return {messaage: `user does not exist`}
            }
            else {
                return err
            }
        }
    })
}

// UPDATE USER INFORMATION
module.exports.update = async (id, reqBody) => {
    const userData = {
        firstName: reqBody.firstName,
        lastname: reqBody.lastName,
        email:  reqBody.email,
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
    }
    return await User.findByIdAndUpdate(id, {$set: reqBody}, {new:true}).then((result,err) => {
        // user ? user : err
        if(result){
            result.password = "***"
            return result
        }
        else {
            return err
        }
    })
}

// UPDATE USER PASSWORD
module.exports.updatePassword = async (id, reqBody) => {
    const userPassword = {
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
    }

    return await User.findByIdAndUpdate(id, {$set: reqBody.password}, {new:true}).then((result,err) => {
        if(result){
            return result
        }
        else{
            return err
        }
    })
}

// Update password mam joy
module.exports.updatePw = async (id, password) => {
    const updatedPw = {
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
    }
    return await User.findByIdAndUpdate(id, {$set: updatePw}).then((result,err) => {
        if(result){
            result.password = "***"
            return result
        }
        else{
            return err
        }
    })
}

// Update user isAdmin status to true
module.exports.adminStatus = async (reqBody) => {
    return await findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin:true}}).then((result,err)=>{
        if(result){
            return true
        }
        else{
            return err
        }
    })
}

// Update user isAdmin status to false
module.exports.isUser = async (reqBody) => {
    return await findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin:false}}).then((result,err)=>{
        if(result){
            return true
        }
        else{
            return err
        }
    })
}

module.exports.deleteUser = async (reqBody) => {
    return await findOneAndDelete({email: reqBody.email}).then((result,err) => {
        result ? true : err
    })
}