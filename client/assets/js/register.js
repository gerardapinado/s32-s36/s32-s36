// target the Element
const registerForm = document.getElementById('register')

// element.addEventListener("event", () => {})
// "submit" -> event that form is submitted
registerForm.addEventListener("submit", (e) => {
    // Event -> creates an big object "event object"
    e.preventDefault() // prevent from reloading the screen
    // console.log(e)

    //get the values of the input fields
    const fn = document.getElementById('firstname').value
    const ln = document.getElementById('lastname').value
    const email = document.getElementById('email').value
    const pw = document.getElementById('password').value
    const cpw = document.getElementById('cpw').value
    
    // console.log(fn, ln, email, pw, cpw)

    //check match pw == cpw
    if(pw===cpw){
        // send the request to the server
            // fetch(URL, {options})
        fetch(`http://localhost:3007/api/users/email-exists`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                firstName: fn,
                lastName: ln,
                email: email,
                password: pw
            })
        })
        .then(result => result.json())
        .then(result => {
            // console.log(result)
            if(result == false){
                // console.log(result)
                // if email not existing in DB, send a request to register the user
                fetch(`http://localhost:3007/api/users/register`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify ({
                    firstName: fn,
                    lastName: ln,
                    email: email,
                    password: pw
                    })
                })
                .then(result => result.json())
                .then(result => {
                    // console.log(result) // boolean
                    if(result){
                        alert(`User successfully registered!`)
                        // redirect the page to login.html
                        window.location.replace('./login.html')
                    }
                    else{
                        alert(`Please try again!`)
                    }
                })

            }
            else{
                alert(`User Already exist`)
            }
        })
    }
})
// get the values of the input fields

// send the request to the server